<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai&display=swap" rel="stylesheet">

    <title>Document</title>
</head>
<body>
<style>
  
  body { 
 background-color:		#CD5C5C;
  margin: 0;
  font-family: 'IBM Plex Sans Thai', sans-serif;
}


.header {
  overflow: hidden;
  background-color: #800000;
  padding:10px 10px;
  font-family: 'IBM Plex Sans Thai', sans-serif;
}
.red-box {
  		background:#000	;
          padding:8px 118px;}

h1{
    overflow: hidden;
    padding:8px 118px;
    color:#FFF;
    font: 2em sans-serif;
    font-family: 'IBM Plex Sans Thai', sans-serif;
}
h5{
    backgroung :#
    color :#fff
    font-family: 'IBM Plex Sans Thai', sans-serif;
}
p{ 
  font-family: 'IBM Plex Sans Thai', sans-serif;
   font: 2em sans-serif;  
}
.hstack {
  color:#FFF;
  
  font-family: 'IBM Plex Sans Thai', sans-serif;
  
}
.box {
  		background:#800000	;
          padding:30px 118px;}

a:link {text-decoration: none}
h4{
  
   font: 25px sans-serif;
   font-family: 'IBM Plex Sans Thai', sans-serif;  
   color:#fff

   
 
}
h3{
  font-family: 'IBM Plex Sans Thai', sans-serif;
    color :#fff
    
    
;
}
li{
  font: 23px sans-serif;  
  font-family: 'IBM Plex Sans Thai', sans-serif;
}
.stack {
  color:#FFF;
  font: 25px sans-serif;
  font-family: 'IBM Plex Sans Thai', sans-serif;
}
</style>
</body>
<div class="header">
   <img src="https://www.npru.ac.th/2019/img/logo.png "alt="alternatetext">
   <div class="hstack gap-2">
    <div class=" border-10px ms-auto">TH</div>
 <div class="vr"></div>
 <div class=" border-10px color-#fff">EN</div>
</div>
  </div>
  </div>
   <div class="red-box"></div> 
   <div class="box">
 
   <div class="hstack gap-3">

  <div class="link ms-auto"><a href="index" class="link-light"><h4>หน้าหลัก</h4></a></div>
  <div class="vr"></div>
  <div class="link"><a href="signin" class="link-light"><h4>สมัครเรียน</h4></a></div>

   </div>
   </div><br>
   <div class="container-full">
    <center> <h4>แนะนำหลักสูตร</h4><span class="badge bg-secondary">สาขาวิชาทั้งหมด</span></center><br>
   <hr>
  

    <div class="container">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home" class="link-dark">ปริญญาตรี (ภาคปกติ)</a></li>
    <li><a data-toggle="tab" href="#menu1"class="link-dark">ปริญญาตรี ภาคกศ.พป.(เรียนวันอาทิตย์)</a></li>

  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <ul>
      <h3>ครุศาสตร์</h3>
      <li><a href="#" class="link-light">เทคโนโลยีและนวัตกรรมการศึกษา</a></li>
      <li><a href="#" class="link-light"> การศึกษาปฐมวัย </a></li>
      <li><a href="#" class="link-light">  พลศึกษา   </a></li>
      <li><a href="#" class="link-light"> การประถมศึกษา   </a></li>
     <hr>      
      <h3>วิทยาศาสตร์และเทคโนโลยี </h3>
      <li><a href="#" class="link-light">จุลชีววิทยา  </a></li>
      <li><a href="#" class="link-light"> เทคโนโลยีการผลิตพืช </a></li>
      <li><a href="#" class="link-light">  สาธารณสุขศาสตร์     </a></li>
      <li><a href="#" class="link-light"> วิทยาการคอมพิวเตอร์  </a></li>
      <li><a href="#" class="link-light">  เคมีเวชสำอาง     </a></li>
      <li><a href="#" class="link-light"> เทคโนโลยีสารสนเทศ    </a></li></ul>
      <li><a href="#" class="link-light">เทคโนโลยีมัลติมีเดีย   </a></li>
      <li><a href="#" class="link-light"> เทคโนโลยีคอมพิวเตอร์  </a></li>
      <li><a href="#" class="link-light">วิศวกรรมโยธา  </a></li>
      <li><a href="#" class="link-light"> การจัดการอาหาร   </a></li>
      <li><a href="#" class="link-light"> อาชีวอนามัยและความปลอดภัย  </a></li>
      <li><a href="#" class="link-light">วิศวกรรมไฟฟ้า    </a></li>
      <li><a href="#" class="link-light"> วิทยาการข้อมูล   </a></li>
      <li><a href="#" class="link-light"> วิศวกรรมซอฟต์แวร์  </a></li>
      <li><a href="#" class="link-light">  คอมพิวเตอร์ศึกษา  </a></li>
      <li><a href="#" class="link-light"> ฟิสิกส์     </a></li>
      <li><a href="#" class="link-light">เคมี  </a></li>
      <li><a href="#" class="link-light"> ชีววิทยา   </a>
      <li><a href="#" class="link-light"> อุตสาหกรรมศิลป์     </a></li>
      <li><a href="#" class="link-light">  วิทยาศาสตร์ทั่วไป   </a></li>
      <hr>   
      <h3>มนุษยศาสตร์และสังคมศาสตร์</h3>
      <li><a href="#" class="link-light">ภาษาอังกฤษธุรกิจ  </a></li>
      <li><a href="#" class="link-light">การพัฒนาชุมชน  </a></li>
      <li><a href="#" class="link-light">ออกแบบดิจิทัลอาร์ต  </a></li>
      <li><a href="#" class="link-light">ออกแบบนิเทศศิลป์   </a></li>
      <li><a href="#" class="link-light"> รัฐประศาสนศาสตร์  </a></li>
      <li><a href="#" class="link-light">นิติศาสตร์      </a></li></ul>
      <li><a href="#" class="link-light">ภาษาไทยเพื่ออาชีพ  </a></li>
      <li><a href="#" class="link-light">การจัดการการบริการ การท่องเที่ยว และสุขภาพนานาชาติ  </a></li>
      <li><a href="#" class="link-light"> การท่องเที่ยวและอุตสาหกรรมบริการ  </a></li>
      <li><a href="#" class="link-light"> สารสนเทศดิจิทัลและบรรณารักษศาสตร์  </a></li>
      <li><a href="#" class="link-light"> อาชีวอนามัยและความปลอดภัย  </a></li>
      <li><a href="#" class="link-light">นาฏศิลป์และศิลปะการแสดง  </a></li>
      <li><a href="#" class="link-light">สังคมศึกษา  </a></li>
      <li><a href="#" class="link-light">ภาษาไทย  </a></li>
      <li><a href="#" class="link-light">ภาษาอังกฤษ  </a></li>
      <li><a href="#" class="link-light"> ดนตรีศึกษา      </a></li>
      <li><a href="#" class="link-light"> ภาษาจีน   </a></li>
      <li><a href="#" class="link-light">ศิลปศึกษา  </a></li>
      <hr>   
      <h3> วิทยาการจัดการ  </h3>
      <li><a href="#" class="link-light">การจัดการทั่วไป  </a></li>
      <li><a href="#" class="link-light">การตลาด  </a></li>
      <li><a href="#" class="link-light">การจัดการทรัพยากรมนุษย์  </a></li>
      <li><a href="#" class="link-light"> ธุรกิจระหว่างประเทศ  </a></li>
      <li><a href="#" class="link-light"> การจัดการโลจิสติกส์และโซ่อุปทาน   </a></li>
      <li><a href="#" class="link-light"> การบัญชี  </a></li></ul>
      <li><a href="#" class="link-light">การเงินและการลงทุน  </a></li>
      <li><a href="#" class="link-light">ธุรกิจดิจิทัล  </a></li>
      <li><a href="#" class="link-light"> นิเทศศาสตร์ (การสื่อสารการตลาดดิจิทัล)   </a></li>
      <li><a href="#" class="link-light"> นิเทศศาสตร์ (วิทยุโทรทัศน์มัลติแพลตฟอร์ม)  </a></li>
      <hr>   
      <h3>พยาบาลศาสตร์    </h3>
      <li><a href="#" class="link-light"> พยาบาลศาสตร์  </a></li>
    </div></ul>
    <div id="menu1" class="tab-pane fade">
    <ul>
      <h3> 	 วิทยาศาสตร์และเทคโนโลยี   </h3>
      <li><a href="#" class="link-light">เทคโนโลยีการผลิตพืช   </a></li>
      <li><a href="#" class="link-light">  สาธารณสุขศาสตร์  </a></li>
      <li><a href="#" class="link-light"> เทคโนโลยีสารสนเทศ    </a></li>
      <li><a href="#" class="link-light">  เทคโนโลยีคอมพิวเตอร์  </a></li>
      <li><a href="#" class="link-light">  วิศวกรรมไฟฟ้า   </a></li>
      <hr>
      <h3> มนุษยศาสตร์และสังคมศาสตร์   </h3>
      <li><a href="#" class="link-light">การพัฒนาชุมชน </a></li>
      <li><a href="#" class="link-light">  รัฐประศาสนศาสตร์  </a></li>
      <li><a href="#" class="link-light"> นิติศาสตร์   </a></li>
      <hr>    
      <h3>  	 วิทยาการจัดการ </h3>
      <li><a href="#" class="link-light">การจัดการทั่วไป   </a></li>
      <li><a href="#" class="link-light"> การตลาด  </a></li>
      <li><a href="#" class="link-light"> การบัญชี    </a></li>
      <li><a href="#" class="link-light">ธุรกิจดิจิทัล  </a></li>
      <li><a href="#" class="link-light"> นิเทศศาสตร์ (การสื่อสารการตลาดดิจิทัล)  </a></li>
      <li><a href="#" class="link-light">นิเทศศาสตร์ (วิทยุโทรทัศน์มัลติแพลตฟอร์ม)   </a></li>
      </ul>
     
    </div>
    <div id="menu2" class="tab-pane fade">
    
    </div>
    <div id="menu3" class="tab-pane fade">
      
    </div>
  </div>
</div>
