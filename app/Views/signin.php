<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai&display=swap" rel="stylesheet">
    <title>Codeigniter Login with Email/Password Example</title>
    <style>
    body { 
 background-color:		#CD5C5C;
  margin: 0;
  font-family: 'IBM Plex Sans Thai', sans-serif;
}

.header {
  overflow: hidden;
  background-color: #800000;
  padding:10px 10px;
}
.red-box {
  		background:#000	;
          padding:8px 118px;
  	}
  p{
      background:  #8B0000	;
          padding:8px 8px;
          color: #FFFFFF;
          border: 1px solid white;
          font: 15px sans-serif;
     
    }
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 230px;
    height: 800px;
    background-color: #8B0000;
    font: 15px sans-serif;
    
}

li a {
    display: block;
    color: white;
    padding: 25px   30px 30px;
    text-decoration: none;
}
li a:hover {
    font-family: 'IBM Plex Sans Thai', sans-serif;
    background-color: #8B0000;
    color: white;
}
ul {
    font-family: 'IBM Plex Sans Thai', sans-serif;
    border: 1px solid white;
}
li {
    font-family: 'IBM Plex Sans Thai', sans-serif;
    border-bottom: 1px solid white;
}
.container{
    font-family: 'IBM Plex Sans Thai', sans-serif;
  position: absolute;
  top: 4em;

  color:white;
  background:#800000;
  padding: 3px 10px 90px 10px;
  height: 100; width: 670px;
  margin: 100px 1220px 1160px 490px;
}


</style>
</head>
<body>
<div class="header">
   <img src="https://www.npru.ac.th/2019/img/logo.png "alt="alternatetext"  ></div>
   <div class="red-box"></div>
    
    <div class="box">
      
<ul>
<li><a href="index" title="Register">กลับ</a></li>

  
<li><a href="signup" title="Register">ลงทะเบียนเข้าใช้ระบบ</a></li>

&nbsp;
<li><a href="" title="Quota List" >สาขาและจำนวนที่รับสมัคร</a></li>&nbsp;
<li><a href="">ตอบคำถาม</a></li><lb>&nbsp;</lb><li><a href="" target="_blank">รายงานการรับสมัคร</a></li>
<li><a href="" target="_blank">ขั้นตอนการสมัคร</a></li>
</ul>
</div>
</td>
  </head>
  <body>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-5">
                <br>
                <h2>เข้าสู่ระบบ</h2><hr><br>
                
                <?php if(session()->getFlashdata('msg')):?>
                    <div class="alert alert-warning">
                       <?= session()->getFlashdata('msg') ?>
                    </div>
                <?php endif;?>
                <form action="<?php echo base_url(); ?>/SigninController/loginAuth" method="post">
                    <div class="form-group mb-3">
                        <input type="idcard" name="idcard" placeholder="เลขบัตรประชาชน 13 หลัก" value="<?= set_value('idcard') ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                        <input type="password" name="password" placeholder="Password" class="form-control" >
                    </div>
                    
                    <div class="d-grid">
                         <button type="submit" class="btn btn-info">เข้าสู่ระบบ</button>
                    </div>     
                </form>
            </div>
              
        </div>
    </div>
  </body>
</html>