<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai&display=swap" rel="stylesheet">
    <title>Codeigniter Auth User Registration Example</title>
    <style>
body { 
 background-color:		#CD5C5C;
  margin: 0;
  font-family: 'IBM Plex Sans Thai', sans-serif;
}

.header {
  overflow: hidden;
  background-color: #800000;
  padding:10px 10px;
}
.container{
   font-family: 'IBM Plex Sans Thai', sans-serif;
  position: absolute;
  top: 8em;

  color:white;
  background:#800000;
  padding: 12px 15px 10px 10px;
  height: 5; width:5;
  margin: 10px 10px 6px 350px;
}
p{
      background:  #8B0000	;
          padding:8px 8px;
          color: #FFFFFF;
          border: 1px solid white;
          font-family: 'IBM Plex Sans Thai', sans-serif;
          font: 15px sans-serif;
     
    }
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 230px;
    height: 800px;
    background-color: #8B0000;
    font-family: 'IBM Plex Sans Thai', sans-serif;
    font: 15px sans-serif;
    
}

li a {
    display: block;
    color: white;
    padding: 25px   30px 30px;
    text-decoration: none;
}
li a:hover {
    background-color: #8B0000;
    color: white;
}
ul {
    border: 1px solid white;
}
li {
  font-family: 'IBM Plex Sans Thai', sans-serif;
    border-bottom: 1px solid white;
}
.red-box {
  font-family: 'IBM Plex Sans Thai', sans-serif;
  		background:#000	;
          padding:8px 118px;}
</style>   
</head>
<body>
<div class="header">
   <img src="https://www.npru.ac.th/2019/img/logo.png "alt="alternatetext"  ></div>
   <div class="box">
   <div class="red-box"></div>  
      <ul>
   
      <li><a href="index" title="index">กลับ</a></li>

      <li><a href="signin" title="Login">เข้าสู่ระบบ</a></li>
      &nbsp;
      <li><a href="" title="Quota List" >สาขาและจำนวนที่รับสมัคร</a></li>&nbsp;
      <li><a href="">ตอบคำถาม</a></li><lb>&nbsp;</lb><li><a href="" target="_blank">รายงานการรับสมัคร</a></li>
      <li><a href="" target="_blank">ขั้นตอนการสมัคร</a></li>
      </ul>
      </div>
      </td>
</div>
    <div class="container mt-8">
        <div class="row justify-content-md-center">
            <div class="col-6">
                <h2>ลงทะเบียนผู้สมัคร</h2><hr>
                <?php if(isset($validation)):?>
                <div class="alert alert-warning">
                   <?= $validation->listErrors() ?>
                </div>
                <?php endif;?>
                <form action="<?php echo base_url(); ?>/ProfileController/store" method="post">
                <h5>กรุณากรอกข้อมูลให้ครบทั้งหมด*</h5>
                <div class="row g-3">
                <div class="col-md-4">
                <label>สัญชาติ</label>
            <select name="nationality" id="nationality" class="form-control">
              <option value="">เลือกสัญชาติ</option>
              <option value="th">ไทย</option>
              <option value="๋่JP">ญี่ปุน</option>
              </select>
  </div>
  <div class="col-md-3">
<label>Gender</label>
            <select name="gender" id="gender" class="form-control">
              <option value="">Select Gender</option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
  </div>
 </div><br>
 <div class="row g-3">
  <div class="col">
  <label for="fname" class="form-label">ชื่อ</label>
  <input type="text" id = "fname" name="fname" placeholder="Name" value="<?= set_value('fname') ?>" class="form-control" >
  </div>
  <div class="col">
  <label for="lname" class="form-label">นามสกุล</label>
  <input type="text" id="lname" name="lname" placeholder="Name" value="<?= set_value('lname') ?>" class="form-control" >
  </div>
</div><br>
<div class="row g-3">
  <div class="col">
 <label for="idcard" class="form-label">บัตรประชาชน</label>
  <input type="text" name="idcard" placeholder="idcard" value="<?= set_value('idcard') ?>" class="form-control" >
  </div>
  <div class="col">
 <label for="email" class="form-label">Email</label>
  <input type="email" name="email" placeholder="email" value="<?= set_value('email') ?>" class="form-control" >
  </div>
  </div><br>
  <div class="row g-8">
  <div class="col-5">
  <label for="birthday">วันเดือนปีเกิด:</label>
  <input type="date" id="birthday" name="birthday"value="<?= set_value('birthday') ?>" class="form-control"> </div>
   <div class="col-5">
    <label for="religion" class="form-label">ศาสนา</label>
    <select id="religion" class="form-select">
      <option selected>พุทธ</option>
      <option>อิสลาม</option>
    </select>
  </div>
</div><br><hr>
<h5>ข้อมูลที่อยู่*</h5>
<div class="row g-3">
  <div class="col-5">
  <label for="housenumber" class="form-label">บ้านเลขที่</label>
  <input type="text" name="housenumber" placeholder="housenumber" value="<?= set_value('housenumber') ?>" class="form-control" >
  </div>
  <div class="col-5">
  <label for="village" class="form-label">หมู่</label>
  <input type="text" name="village" placeholder="village" value="<?= set_value('village') ?>" class="form-control" >
  </div>
</div><br>
<div class="row g-3">
  <div class="col-5">
  <label for="land" class="form-label">ซอย</label>
  <input type="text" name="land" placeholder="land" value="<?= set_value('land') ?>" class="form-control" >
  </div>
  <div class="col-5">
  <label for="road" class="form-label">ถนน</label>
  <input type="text" name="road" placeholder="road" value="<?= set_value('road') ?>" class="form-control" >
  </div>
</div><br>
<div class="row g-4">
<div class="col-md-5">
 <label>ตำบล</label>
            <select name="canton" id="canton" class="form-control">
              <option value="">Select Gender</option>
              <option value="tk">Male</option>
              <option value="mk">Female</option>
            </select>
  </div>
<div class="col-md-5">
<label for="district" class="form-label">เขต/อำเภอ</label>
    <select id="district" class="form-select">
      <option selected>เมือง</option>
      <option>หญิง</option>
    </select>
  </div>
<div class="col-md-5">
    <label for="province" class="form-label">จังหวัด</label>
    <select id="province" class="form-select">
      <option selected>นครปฐม</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-5">
  <label for="postcode" class="form-label">รหัสไปรษณีย์</label>
  <input type="text" name="postcode" placeholder="postcode" value="<?= set_value('postcode') ?>" class="form-control" >
  </div>

 </div><br>
  <div class="col-5">
  <label for="telphone" class="form-label">เบอร์โทรศัพท์</label>
  <input type="tel" name="telphone" placeholder="telphone" value="<?= set_value('telphone') ?>" class="form-control" >
  </div>
 </div>
 <div class="d-grid-m5"><br><br>
                        <center><button type="submit" class="btn btn-warning">บันทึกข้อมูลลงทะเบียน</button>
                    </div>