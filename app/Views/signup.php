<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai&display=swap" rel="stylesheet">
    <title>Codeigniter Auth User Registration Example</title>
    <style>
body { 
 background-color:		#CD5C5C;
  margin: 0;
  font-family: 'IBM Plex Sans Thai', sans-serif;
}

.header {
  overflow: hidden;
  background-color: #800000;
  padding:10px 10px;
}
.container{
  
  position: absolute;
  top: 8em;

  color:white;
  background:#800000;
  padding: 12px 15px 10px 10px;
  height: 5; width:5;
  margin: 10px 10px 6px 350px;
}
p{    font-family: 'IBM Plex Sans Thai', sans-serif;
      background:  #8B0000	;
          padding:8px 8px;
          color: #FFFFFF;
          border: 1px solid white;
          font: 15px sans-serif;
     
    }
  ul {
    font-family: 'IBM Plex Sans Thai', sans-serif;
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 230px;
    height: 800px;
    background-color: #8B0000;
    font: 15px sans-serif;
    
}

li a {
    font-family: 'IBM Plex Sans Thai', sans-serif;
    display: block;
    color: white;
    padding: 25px   30px 30px;
    text-decoration: none;
}
li a:hover {
    background-color: #8B0000;
    color: white;
}
ul {
    border: 1px solid white;
}
li {
   
    border-bottom: 1px solid white;
}
.red-box {
  		background:#000	;
          padding:8px 118px;}
</style>   
</head>
<body>
<div class="header">
   <img src="https://www.npru.ac.th/2019/img/logo.png "alt="alternatetext"  ></div>
   <div class="box">
   <div class="red-box"></div>  
      <ul>
   
      <li><a href="index" title="index">กลับ</a></li>

      <li><a href="signin" title="Login">เข้าสู่ระบบ</a></li>
      &nbsp;
      <li><a href="" title="Quota List" >สาขาและจำนวนที่รับสมัคร</a></li>&nbsp;
      <li><a href="">ตอบคำถาม</a></li><lb>&nbsp;</lb><li><a href="" target="_blank">รายงานการรับสมัคร</a></li>
      <li><a href="" target="_blank">ขั้นตอนการสมัคร</a></li>
      </ul>
      </div>
      </td>
</div>
<br>
    <div class="container mt-5">
        <div class="row justify-content-md-center">
            <div class="col-6">
            <br><br> <h2>ลงทะเบียนผู้สมัคร</h2><hr>
                <?php if(isset($validation)):?>
                <div class="alert alert-warning">
                   <?= $validation->listErrors() ?>
                </div>
                <?php endif;?>
                <form action="<?php echo base_url(); ?>/SignupController/store" method="post">
                    <div class="form-group mb-3">
                    <label for="inputname" class="form-label">ชื่อ</label>
                        <input type="text" name="name" placeholder="Name" value="<?= set_value('name') ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                    <label for="lname" class="form-label">นามสกุล</label>
                        <input type="text" name="lname" placeholder="lName" value="<?= set_value('lname') ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                        <label for="idcard" class="form-label">เลขบัตรประชาชน</label>
                        <input type="text" name="idcard"placeholder="เลขบัตรประชาชน 13 หลัก" class="form-control"  value="<?= set_value('idcard'); ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                    <label for="inputemail" class="form-label">Email</label>
                        <input type="email" name="email" placeholder="Email" value="<?= set_value('email') ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                    <label for="inputpassword" class="form-label">รหัสผ่าน</label>
                        <input type="password" name="password" placeholder="Password" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                    <label for="inputpasswordd" class="form-label">ยืนยันรหัสผ่าน</label>
                        <input type="password" name="confirmpassword" placeholder="Confirm Password" class="form-control" >
                    </div>
                    <div class="d-grid-m3"><br>
                        <center><button type="submit" class="btn btn-warning">ลงทะเบียน</button><br>

                        <br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>