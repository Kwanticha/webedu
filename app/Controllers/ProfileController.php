<?php 
namespace App\Controllers;  
use CodeIgniter\Controller;

class ProfileController extends Controller

{
    public function index()
    {
       
        helper(['form']);
        
        $data = [];
       
        echo view('register', $data);
        
    }
    
    public function store()
    {
        helper(['form']);
      
        $rules = [
           
        
        'fname' => 'required|min_length[1]|max_length[50]',
        'lname'  => 'required|min_length[1]|max_length[50]',
        
        ];
          
        if($this->validate($rules)){
            
            $profilemodel = new profilemodel();
            $data = [
                
                'fname' => $this->request->getVar('fname'),
                'lname' => $this->request->getVar('lname')
                
            ];
            
            $profilemodel -> save($data);
           
             return redirect()->to('/myprofile');
        }else{
            $data['validation'] = $this->validator;
            
            echo view('register', $data);
        }
          
    }
  
}
